<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class events extends Controller
{
    public function addevent(Request $request)
    {
        DB::table('events')->insert(
            [
                'event_name' => $request->event_name,
                'event_start' => $request->event_start,
                'event_end' => $request->event_end,
                'event_days' => $request->event_days,
                'created_at' => Carbon::now()->format('Y-m-d'),
                'updated_at' => Carbon::now()->format('Y-m-d'),
            ]
        );
        return $request;
    }

    public function getlastevent(Request $request)
    {
        $lastentry = DB::table('events')->latest('id')->first();
        return response()->json($lastentry);
    }
}
