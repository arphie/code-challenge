<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::middleware('auth:api')->post('/add', function (Request $request) {
//     return 'Hello World';
// });


Route::get('/foo', function () {
    return 'Hello World';
});

Route::post('/addevent', 'events@addevent');
Route::post('/lastevent', 'events@getlastevent');



// Route::middleware([“auth”])->group(function () {
//     Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword'); 
// });