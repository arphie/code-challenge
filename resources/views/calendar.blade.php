<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

        <!-- Styles -->
        <style>
            .dmaincontent {
                width: 980px;
                margin: 0 auto;
            }
            .isdates {
                margin: 0;
                padding: 0;
            }
            .isdates li {
                display: inline-block;
                list-style: none;
                padding: 0;
                margin: 0;
            }
            .contentpart ul li{
                padding: 0;
                margin: 0;
                list-style: none;
            }
            .contentpart ul li.eventlister {
                padding: 5px 10px;
                border-bottom: 1px solid gainsboro;
            }
            .contentpart ul li.dmonth {
                font-weight: bold;
                font-size: 18px;
                margin: 20px 0 10px;
            }
            .contentpart ul li.eventlister.hasevent {
                background: #bcf7d6;
            }
            .eventdname {
                font-weight: bold;
                font-size: 16px;
            }
            .eventdate {
                font-size: 12px;
            }
            span.dayname {
                font-style: italic;
            }
        </style>
    </head>
    <body>
        <div class="dmaincontent">
            <div class="col-md-5">
                <div class="formpart">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Event Name</label>
                            <input type="email" class="form-control" id="event_name" aria-describedby="emailHelp" placeholder="Enter Event Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">From</label>
                            <input type="date" class="form-control" id="event_start" aria-describedby="emailHelp" placeholder="Enter Event Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">To</label>
                            <input type="date" class="form-control" id="event_end" aria-describedby="emailHelp" placeholder="Enter Event Name">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <ul class="isdates">
                            <li><input type="checkbox" name="ddate[]" value="mon">Mon</li>
                            <li><input type="checkbox" name="ddate[]" value="tue">Tue</li>
                            <li><input type="checkbox" name="ddate[]" value="wed">Wed</li>
                            <li><input type="checkbox" name="ddate[]" value="thu">Thu</li>
                            <li><input type="checkbox" name="ddate[]" value="fri">Fri</li>
                            <li><input type="checkbox" name="ddate[]" value="sat">Sat</li>
                            <li><input type="checkbox" name="ddate[]" value="sun">Sun</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" name="submitevent" name="submit" id="submitevent">
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="dlisrofmonths">
                    <div class="headerpart"></div>
                    <div class="contentpart">
                        <ul></ul>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
            $( document ).ready(function() {

                let init_date = new Date();
                let dmonth = init_date.getMonth();
                var fulldayname = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
                let weekday = ["sun","mon","tue","wed","thu","fri","sat"];
                let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


                let runmonths = function(year, month){
                    let dats = new Date(year, month, 0).getDate();
                    return dats;
                }

                let getevents = function(event){
                    $.ajax({
                        url: "/api/lastevent",
                        type: 'POST',
                        dataType: 'json', // added data type
                        success: function(data) {
                            
                            // console.log(data.event_days);
                            let days = data.event_days;
                            let name = data.event_name;
                            // start event
                            let eventstart = new Date(data.event_start);
                            let eventmonth = (eventstart.getMonth() + 1);
                            let finalstartmonth = (eventmonth < 10 ? "0" + eventmonth : eventmonth);
                            let eventstartyear = eventstart.getFullYear();

                            // end event
                            let eventend = new Date(data.event_end);
                            let eventmonthend = (eventend.getMonth() + 1);
                            let finalendmonth = (eventmonthend < 10 ? "0" + eventmonthend : eventmonthend);
                            let eventendyear = eventend.getFullYear();

                            // let getdates = 

                            for (let index = eventmonth; index <= eventmonthend; index++) {
                                let month = index;
                                let ismonth = (month < 10 ? "0" + month : month);
                                let dayspermonth = runmonths(eventstartyear, ismonth);
                                $(".contentpart > ul").append("<li class='dmonth'>"+months[month - 1]+"</li>");
                                for (let index2 = 1; index2 <= dayspermonth; index2++) {
                                    let curdate = (index2 < 10 ? "0" + index2 : index2);
                                    let dinfs = ismonth+'/'+curdate+'/'+eventstartyear;
                                    let dsamdate = new Date(dinfs);
                                    
                                    let date_name = weekday[dsamdate.getDay()];
                                    
                                    if(days.indexOf(date_name) >= 0){
                                        let devent = "<li class='eventlister hasevent'>"
                                        devent += "<div class='eventdate'>"+months[month - 1]+" "+curdate+", "+eventstartyear+" <span class='dayname'>"+fulldayname[dsamdate.getDay()]+"</span><div>"
                                        devent += "<div class='eventdname'>"+name+"<div>"
                                        devent += "</li>"
                                        $(".contentpart > ul").append(devent);
                                        // console.log(dinfs + " -> "+ date_name +" allowed");
                                    } else {
                                        let devent = "<li class='eventlister'>"
                                        devent += "<div class='eventdate'>"+months[month - 1]+" "+curdate+", "+eventstartyear+" <span class='dayname'>"+fulldayname[dsamdate.getDay()]+"</span><div>"
                                        // devent += "<div class='eventdname'>"+name+"<div>"
                                        devent += "</li>"
                                        $(".contentpart > ul").append(devent);
                                        // console.log(dinfs + " -> "+ date_name +" not allowed");
                                    }

                                    // $(".contentpart > ul").append("<li></li>");
                                }
                                // console.log(dayspermonth);
                            }

                            console.log(finalendmonth + " "+ eventendyear);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            
                        }
                    });
                    // return eventdata;
                }
                new getevents();


                $("#submitevent").click(function(e){ 
                    let event_name = $("#event_name").val();
                    let event_start = $("#event_start").val();
                    let event_end = $("#event_end").val();
                    
                    let info = "";
                    $(':checkbox:checked').each(function(i){
                        info = info+","+ $(this).val()
                    });

                    $.ajax({
                        url: "/api/addevent",
                        type: 'POST',
                        data: {
                            "event_name": event_name,
                            "event_start": event_start,
                            "event_end": event_end,
                            "event_days": info
                        },
                        dataType: 'json', // added data type
                        success: function(data) {
                            $(".contentpart ul").empty();
                            new getevents();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            
                        }
                    });


                    console.log(info);
                });
            });
        </script>
    </body>
</html>
